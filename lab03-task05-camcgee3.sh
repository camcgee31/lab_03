#!/bin/sh

op=$1
i=1
total=0
t=`expr $# - 1`
total1=0
function add {
	total1=`expr $total + $*`
	return $total1
}

function sub {
	for i in $*
	do
		shift
		((sum-=$1))
	done
	return $sum
}

function mul {
	total1=1
	total1=`expr $1 * $total1`
	# take each value check if its 0 if not then mul them
	
	while [ $i -le $* ]
	do
		shift
		if [ $1 -gt 0 ]
		then
		total1=`expr $1 * $2`
		
		((i++))
		fi
	done
	total=$?	
	return $total1
}


function div {
	total1=`expr $total1 / $1`
	return $total1
}

function exp {
	total1=`expr $total ** $1`
	return $total1
}

function mod {
	total1=`expr $total % $1`
	return $total1
}

case $op in
	"add")
		while [ $i -le $t ]
		do
			add $2
			total=$?
			((i++))
			shift
		done

		echo "$total"
		;;
	"sub")
		sub $@
		total=$?
		echo "$total"
		;;
	"mul")
		mul $@
		total=$?
		echo "$total"
		;;
	"div")
		while [ $i -le $t ]
		do
			div $2 $3
			total=$?
			((i++))
			shift
		done

		echo "$total"
		;;
	"exp")
		while [ $i -le $t ]
		do
			exp $2 $3
			total=$?
			((i++))
			shift
		done

		echo "$total"
		;;
	"mod")
		while [ $i -le $t ]
		do
			mod $2 $3
			total=$?
			((i++))
			shift
		done

		echo "$total"
		;;
	*)
		echo "Pick a operator"
esac

